# Blog PHP

This is an exercice to learn to us git fork project. 

This is a project of a 4 people team : 

* Alex Moulinneuf https://gitlab.com/LunakepioFR/blog-php
* Charles Lafont https://gitlab.com/charles-lafont/blog-php
* Florian Lina https://gitlab.com/Jilow/blog-php
* Sofiane Zouaoui https://gitlab.com/Zouzou-ops/blog-php

The main purpose of this exercice is How to proprly work as a team on a git project.

## The Blog

In order to interact with the blog you have to be connected.
When you are log in you can :
* publish an article with a title and a texte
* publish a comment under an article
* you can log off
