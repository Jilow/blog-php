<?php
session_start();
if (isset($_POST['message'])) {
    echo 'works';

  $article_id = $_GET["id"];

  echo $article_id;

  $comment = array();

  $comment['message'] = $_POST['message'];
  $comment['date'] = date("d/m/Y à Hh:i");
  $comment['id'] = $article_id;
  $comment['author'] = $_SESSION['name'];

  $js = file_get_contents('comments.json');

  $js = json_decode($js, true);

  $js[] = $comment;

  $js = json_encode($js);

  file_put_contents('comments.json', $js);

 header('location: index.php');
} else if (isset($_GET['del'])) {
  $comments = file_get_contents('comments.json');
  $comments = json_decode($comments, true);

  $verifier = array();

  for ($i = 0; $i < count($comments); $i++) {
    if ($comments[$i]['id'] != $_GET['del']) {
      $verifier[] = $comments[$i];

    }
  }

  $verifier = json_encode($verifier);
  file_put_contents('comments.json', $verifier);

  header('location: index.php');
}