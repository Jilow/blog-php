<?php
$t = time();
?>
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="main.css?<?php echo $t ?>">
  <title>Document</title>
</head>

<body>
  <div class="glass-body">
    <div class="text">
      <h3>Add an article</h3>
    </div>
    <div class="glass-add">
      <form action="traitement.php" method="POST">
        <label for="title"> title</label></br>
        <input type="text" class="glass" placeholder="Title" name="title"></br></br>
        <label for="message"> Message :</label></br>
        <textarea class="glass" placeholder="Ecrivez un message" name="message" rows="5" cols="40"></textarea></br>
    </div>
    <button type="submit">Submit</button>
    </form>
  </div>
</body>

</html>