<?php
session_start();
$t=time();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" type="text/css" href="main.css?<?php echo $t?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <div class="plus"><a href="addcontent.php" title="Write an article">+</a></div>
  <div class="logout"><a href="login.html"><button>Log out</button></a></div>
  <div class="glass-card">
    <div class="welcome">
      <h1>Welcome</h1>
    </div>

      <?php 
        $messages = file_get_contents('messages.json') ; 
        $messages = json_decode($messages, true) ;

        for($i=0; $i<count($messages); $i++){
        ?>
           <div class="article">
      <div class="top-layer">
        <img src="pdp.png" width="50px"><span class="name">X &nbsp;&nbsp;</span><span class="adress"> @X</span>
      </div>
      <p class="message-content">
        <?php echo$messages[$i]['title']; ?>
      </p>
      <p class="message-content">
        <?php echo$messages[$i]['contenu']; ?>
      </p>
      <p class="message-content">
        <?php echo$messages[$i]['nom']; ?>
      </p>
      <p class="date">
        <?php echo$messages[$i]['date']; ?>
      </p>
      <p class="commentary">
        </br>
        <h4>Comments :</h4>
          <?php
          $comments = file_get_contents('comments.json') ; 
          $comments = json_decode($comments, true) ;
          for($y=0; $y<count($comments); $y++){
            if( $messages[$i]['id'] == $comments[$y]['id']){
          ?>
            <div id="comments">
            <div class="name"><?php echo $comments[$y]['author'];?></div>
            <?php echo $comments[$y]['message'];?>
          </div>
          <?php }
        }?>
      </p>
      <a href="addcomment.php?id=<?=$messages[$i]['id']?>" title="Write a comment"><img src="comment.png" width="35px"style="filter:  brightness(0) invert(1);"></a>
      </div>    
      <?php
    }
    ?>
    </div>
    </br>

  </div>
</body>
</html>