<?php
session_start();
$t = time();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="main.css?<?php echo $t ?>">
  <title>Document</title>
</head>

<body>
<?php
$article_id = $_GET["id"];
?>
  <div class="glass-body">
    <div class="text">
      <h3>Add your comment</h3>
    </div>
    <div class="glass-add">
      <form action="commentprocess.php?id=<?=$article_id;?>" method="POST">
        <label for="message"> Message :</label></br>
        <textarea class="glass" placeholder="Ecrivez un message" name="message" rows="5" cols="40"></textarea></br>
    </div>
    <button type="submit">Submit</button>
    </form>
  </div>
</body>

</html>